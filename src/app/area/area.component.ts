import { Component, OnInit } from '@angular/core';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { AreaService, RegionsService, Regions, UserService, Area } from '../core';
import { MatButtonModule, MatIconModule, MatProgressSpinner, MatCheckboxModule, MatMenuModule, MatCardModule, MatFormFieldModule, MatSpinner} from '@angular/material';
import { FormBuilder, FormGroup, FormControl, ReactiveFormsModule } from '@angular/forms';
@Component({
  selector: 'app-area',
  templateUrl: './area.component.html',
  styleUrls: ['./area.component.css']
})
export class AreaComponent implements OnInit {

	areaForm: FormGroup;
	errors: Object = {};
  constructor(
  	private areaService: AreaService,
  	private regionsService: RegionsService,
  	private router: Router,
    private route: ActivatedRoute,
    private regionService: RegionsService,
    private userService: UserService,
    private fb: FormBuilder
  	) {
  	this.areaForm = this.fb.group({
      code: '',
      description: '',
      region: '',
      contact: '',
      street1: '',
      street2: '',
      city: '',
      postal_code: '',
      phone: '',
      state: '',
      id: '',
    });
  }
  area: Area = {} as Area;
  areas: Array<string> = [];
  isAuthenticated: boolean;
	regions: Array<string> = [];
	isSubmitting = false;

  ngOnInit() {
  	this.fetchData();

  }

  fetchData() {
    this.areaService.getAll()
    .subscribe(area => {
      this.areas = area;
    });

    this.regionsService.getAll().subscribe(
      (region) => {
        this.regions = region;
      }
    );
  }
  reset(){
    this.areaForm.reset();
  }

  delete(area){
    if(confirm("Are you sure to delete "+area.description)) {
      // this.regionService.destroy(region.id);
      this.areaService.destroy(area.id).subscribe(()  =>  {
        this.fetchData();
      });
    }
  }

  populateForm(area){
    this.areaForm.patchValue(area);
  }

  submitForm() {
    this.isSubmitting = false;
    // console.log(this.regionForm.value);

    // update the model
    this.updateArticle(this.areaForm.value);

    // post the changes
    this.areaService.save(this.area).subscribe(()  =>  {
      this.fetchData();
    }
    );
  }

  updateArticle(values: Object) {
    Object.assign(this.area, values);
  }

}
