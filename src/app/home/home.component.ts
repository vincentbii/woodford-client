import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { User, UserService,SessionService, LocationSesion, Profile , RegionsService, Regions, Location } from '../core';
import { concatMap ,  tap } from 'rxjs/operators';
import {MatCardModule, MatSelectChange, MatOption, MatSelect, MatButton} from '@angular/material';
import {MatInputModule} from '@angular/material/input';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
@Component({
  selector: 'app-home-page',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  locationSesion: LocationSesion = {} as LocationSesion;
  region: Regions = {} as Regions;
  currentUser: User;
  locations: Array<string> = [];
  locationSessionsForm: FormGroup;
  errors: Object = {};
  session = [];

  constructor(
    private router: Router,
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private regionService: RegionsService,
    private userService: UserService,
    private sessionService: SessionService
  ) {
    this.locationSessionsForm = this.fb.group({
      user: '',
      location: ''
    });
  }

  isAuthenticated: boolean;

  ngOnInit() {
    // this.sessionService.getSession()
    //   .subscribe((data)  =>  {
    //     this.locationSessionsForm.patchValue(data);
    //     // console.log(data)
    //   })

    this.userService.currentUser.subscribe(
      (userData) => {
        this.currentUser = userData;
      }
    );

    this.regionService.getLocations().subscribe(
      (data) => {
        this.locations = data;
        // console.log(data)
      }
    );
  }

  objChanged(event){
    this.sessionService.save(event, this.currentUser.username)
      .subscribe(res  =>  {
        // console.log(res);
      })
  }

  updateArticle(values: Object) {
    Object.assign(this.locationSesion, values);
  }


}
