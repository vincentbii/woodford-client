import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { 
  MatCardModule,
  MatSelect, 
  MatOption,
  MatFormField,
  MatDialogRef,
  MatDialog,
  MAT_DIALOG_DATA,
  MatDatepicker
   } from '@angular/material';
import { FormBuilder, FormGroup, Validators, FormControl, ReactiveFormsModule } from '@angular/forms';
import { AreaService, User, State, LocationService, SessionService, TimezoneService, Timezone, RegionsService, Regions, UserService, Area } from '../core';
import { LocationFooterComponent } from '../location-footer/location-footer.component';
import { AllLocationsComponent } from '../location/index';
// import { } from '../index';
@Component({
  providers:[LocationFooterComponent, AllLocationsComponent],
  selector: 'app-location-main',
  templateUrl: './location-main.component.html',
  styleUrls: ['./location-main.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class LocationMainComponent implements OnInit {

  stateCtrl = new FormControl();
  filteredStates: Observable<State[]>;
  timezoneCtrl = new FormControl();
  filteredTimezones: Observable<Timezone[]>;
  states =[];
  location: Location = {} as Location;

	locForm: FormGroup;
  timezones = [];
  paymentActions = [];
  currencies = [];
  countries = [];
  cities = [];
  areas = [];
  drops = [];
  locations: any;
  currentLocation: any;
  currentUser: User;
  errorMessage = '';
  action: any;
  default_rate_plans = [];rateCode = [];lblUnitStatusAfterReturn = [];forms = [];
  	constructor(
  		private fb: FormBuilder,
  		private router: Router,
    	private route: ActivatedRoute,
      private regionsService: RegionsService,
      private areaService: AreaService,
      private locationService: LocationService,
      private sessionService: SessionService,
      private userService: UserService,
      private timezoneService: TimezoneService,
      private footerComponent: LocationFooterComponent,
      public dialog: MatDialog
  		) {
  		this.locForm = this.fb.group({
      id:  '',
      location_name: ['', Validators.required],
      country: '',
      convert_rez_to_local_currency: '',
      area_id: '',
      dba_name: '',
      minimum_age: '',
      change_age: '',
      default_rate_plan: {
        id: '',
        name: '',
      },
      franchise_type: '',
      ra_number: '',
      grace_period: '',
      show_grace_period: '',
      property_id: '',
      government_id: '',
      location_email: '',
      days_in_rental_month: '',
      fax_number: '',
      purge_years: '',
      rem_days:'',
      require_refund: '',
      also_apply: '',
      loc_code: ['', Validators.required],
      email_bcc: '',
      payment_due_action_at_edit: '',
      payment_due_action_at_return: '',
      payment_due_action_at_open: '',
      physical_inventory_group: '',
      contact: '',
      street1: '',
      require_payment: '',
      rate_code: {
        id: '',
        name: ''
      },
      street2: '',
      send_rez_email: '',
      send_rez_email_w_form: '',
      city: '',
      postal_code: '',
      phone_number: '',
      state: '',
      currency: '',
      next_day_cutoff: '',
      timezone: '',
      owning_group_code: '',
      lor_early: '',
      fuel_per_km: '',
      default_fuel_level: '',
      quote_expires:'',
      lor_optional_service: '',
      shop_id:'',
      insurance_certificate:'',
      maintenance_hold:'',
      check_maintenance:'',
      max_days_to_backdate_return:'',
      end_of_gl_period:'',
      split_current_gl_period:'',
      fixed_return:'',

    });

      this.filteredStates = this.stateCtrl.valueChanges
      .map(state => state ? this._filterStates(state) : this.states.slice());

      this.filteredTimezones = this.timezoneCtrl.valueChanges
        .map(timezone => timezone ? this._filteredTimezones(timezone) : this.timezones.slice());

  	}

	ngOnInit() {
    
    this.fetchData();
	}

  getAllLocations(): void{
    const dialogRef = this.dialog.open(AllLocationsComponent,{
      width: '1200px',
      height: 'auto'
    });

    dialogRef.afterClosed().subscribe(result => {
      this.resetForm();
      this.action = true;
      this.locForm.patchValue(result);
    });
  }

  resetForm(){
    this.locForm.reset();
  }


  fetchData(){
    this.userService.currentUser.subscribe(
      (userData) => {
        this.currentUser = userData;
      }
    );

    this.locationService.getAll()
      .subscribe(data  =>  {
        this.resetForm();
        this.locations = data;
        this.currentLocation = this.locations[0];
        this.action = true;
        this.locForm.patchValue(this.currentLocation);
        // console.log(this.locations[0])
      });

    this.areaService.getPaymentDueAction()
      .subscribe(res  =>  {
        this.paymentActions = res
        // console.log(this.paymentActions)
      })

      this.areaService.getAll()
        .subscribe(res  =>  {
          this.areas = res;
        });

      this.areaService.getCities()
        .subscribe(res  =>  {
          this.cities = res;
        })

      this.areaService.getStates()
        .subscribe(res  =>  {
          this.states = res;
        })

      this.areaService.getCountries()
        .subscribe(res  =>  {
          this.countries = res;
        })

      this.areaService.getCurrency()
        .subscribe(res  =>  {
          this.currencies = res;
        })

      this.areaService.getOneWayDrops()
        .subscribe(res  =>  {
          this.drops = res;
        }) 

      this.areaService.getTimezones()
        .subscribe(res  =>  {
          this.timezones = res;
          // console.log(this.timezones)
        });

      this.locationService.getDefaultRatePlans()
        .subscribe(data  =>  {
          this.default_rate_plans = data;
        });

      this.locationService.getRateCodes()
      .subscribe(data  =>  {
        this.rateCode = data;
      })

      this.locationService.getlblUnitStatusAfterReturn()
      .subscribe(data  =>   {
        this.lblUnitStatusAfterReturn = data;
      })

      this.areaService.getsFormSetup(0)
      .subscribe(data  =>  {
        this.forms = data;
      });
  }

  private _filterStates(value: string): State[] {
    const filterValue = value.toLowerCase();
    return this.states.filter(state => state.name.toLowerCase().indexOf(filterValue) === 0);
  }

  private _filteredTimezones(value: string): Timezone[]{
    const filterValue = value.toLowerCase();
    return this.timezones.filter(timezone => timezone.abbreviation.toLowerCase().indexOf(filterValue) == 0);
  }

  newLocation(){
    this.action = false;
    this.errorMessage = '';
    this.resetForm();
  }

  openTaxes(){
    this.footerComponent.openTaxes();
  }

  openOneWayDrops(){
    this.footerComponent.openOneWayDrops();
  }

  fleetAvailability(){
    this.footerComponent.fleetAvailability();
  }

  locationNotes(){
    this.footerComponent.locationNotes();
  }

  formSetup(){
    this.footerComponent.formSetup();
  }

  estimated_monthly_expenses(){
    this.footerComponent.estimated_monthly_expenses();
  }

  submitForm() {
    this.errorMessage = '';
    this.updateArticle(this.locForm.value);
    // console.log(this.location)
    this.areaService.saveLocation(this.locForm.value)
      .subscribe(
        res => {
          this.locForm.reset();
          // this.fetchData();
          this.action = true;
          this.currentLocation = res;
          this.locForm.patchValue(res);
          // console.log(res)
        },
        err  => {
          this.errorMessage = err.errors;
          console.log(err.errors)
        }
        );
      // this.isSubmitting = false;
  }

  updateArticle(values: Object) {
    Object.assign(this.location, values);
  }

}
