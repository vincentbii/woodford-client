import { Component, OnInit, EventEmitter } from '@angular/core';
import {MatCardModule} from '@angular/material/card';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { TaxesMainComponent } from '../taxes-main/taxes-main.component';
import { OneWayDropsComponent } from '../location/one-way-drops/one-way-drops.component';
import { FleetAvailabilityComponent } from '../location/fleet-availability/fleet-availability.component';
import { LocationNotesComponent }from '../location/location-notes/location-notes.component';
import { FormSetupComponent } from '../location/form-setup/form-setup.component';
import { EstimatedMonthlyExpensesComponent } from '../location/estimated-monthly-expenses/estimated-monthly-expenses.component';
@Component({
  selector: 'app-location-footer',
  templateUrl: './location-footer.component.html',
  styleUrls: ['./location-footer.component.css']
})
export class LocationFooterComponent implements OnInit {

  // @Output() myEvent = new EventEmitter();

  constructor(public dialog: MatDialog) { }

  ngOnInit() {
  }

  openTaxes(): void{
  	const dialogRef = this.dialog.open(TaxesMainComponent, {
      width: '1200px',
      height: '600px'
    });
  }

  openOneWayDrops():void {
    const dropsDialog = this.dialog.open(OneWayDropsComponent, {
      width: '1200px',
      height: '600px'
    });
  }

  fleetAvailability():void{
    const fleetDialog = this.dialog.open(FleetAvailabilityComponent, {
      width: '1200px',
      height: '500px'
    });
  }

  locationNotes():void{
    const locationNotesDialog = this.dialog.open(LocationNotesComponent, {
      width: '1000px',
      height: '500px'
    });
  }

  formSetup(): void{
    const formSetupDialog = this.dialog.open(FormSetupComponent, {
      width: '1200px',
      height: '400px'
    });
  }

  estimated_monthly_expenses(): void{
    const estimated_monthly_expensesDialog = this.dialog.open(EstimatedMonthlyExpensesComponent, {
      width: '1200px',
      height: '400px'
    });
  }

  newLocation(){
    
  }
}
