import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LocationFooterComponent } from './location-footer.component';

describe('LocationFooterComponent', () => {
  let component: LocationFooterComponent;
  let fixture: ComponentFixture<LocationFooterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LocationFooterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LocationFooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
