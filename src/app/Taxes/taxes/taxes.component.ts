import { Component, OnInit } from '@angular/core';
import { 
	MatCardModule,
	MatSelect, 
	MatOption, 
	MatDialog, 
	MatDialogRef
	 } from '@angular/material';
export interface Food {
  value: string;
  viewValue: string;
}
@Component({
  selector: 'app-taxes',
  templateUrl: './taxes.component.html',
  styleUrls: ['./taxes.component.css']
})
export class TaxesComponent implements OnInit {

  foods: Food[] = [
	    {value: 'Tax-1', viewValue: 'Tax 1'},
	    {value: 'Tax-2', viewValue: 'Tax 2'},
	    {value: 'Tax-3', viewValue: 'Tax 3'},
	    {value: 'Tax-4', viewValue: 'Tax 4'},
	    {value: 'Tax-5', viewValue: 'Tax 5'},
	    {value: 'Tax-6', viewValue: 'Tax 6'},
	    {value: 'Tax-7', viewValue: 'Tax 7'},
	    {value: 'Tax-8', viewValue: 'Tax 8'},
	    {value: 'Tax-9', viewValue: 'Tax 9'},
	    {value: 'Tax-10', viewValue: 'Tax 10'},
	    {value: 'Tax-11', viewValue: 'Tax 11'},
	    {value: 'Tax-12', viewValue: 'Tax 12'},
	    {value: 'Tax-13', viewValue: 'Tax 13'},
	    {value: 'Tax-14', viewValue: 'Tax 14'},
	    {value: 'Tax-15', viewValue: 'Tax 15'}
	  ];


  constructor(public dialogRef: MatDialogRef<TaxesComponent>) { }

  ngOnInit() {

  }

  close(): void {
    this.dialogRef.close();
  }

}
