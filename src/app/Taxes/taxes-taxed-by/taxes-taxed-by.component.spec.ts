import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxesTaxedByComponent } from './taxes-taxed-by.component';

describe('TaxesTaxedByComponent', () => {
  let component: TaxesTaxedByComponent;
  let fixture: ComponentFixture<TaxesTaxedByComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaxesTaxedByComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxesTaxedByComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
