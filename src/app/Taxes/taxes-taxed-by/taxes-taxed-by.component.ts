import { Component, OnInit } from '@angular/core';
import { 
	MatCardModule,
	MatSelect, 
	MatOption, 
	MatDialog, 
	MatDialogRef
	 } from '@angular/material';
import { TaxesComponent } from '../taxes/taxes.component';

export interface Food {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-taxes-taxed-by',
  templateUrl: './taxes-taxed-by.component.html',
  styleUrls: ['./taxes-taxed-by.component.css']
})
export class TaxesTaxedByComponent implements OnInit {


  foods: Food[] = [
	    {value: 'Taxed By Tax-1', viewValue: 'Taxed By Tax 1'},
	    {value: 'Taxed By Tax-2', viewValue: 'Taxed By Tax 2'},
	    {value: 'Taxed By Tax-3', viewValue: 'Taxed By Tax 3'},
	    {value: 'Taxed By Tax-4', viewValue: 'Taxed By Tax 4'},
	    {value: 'Taxed By Tax-5', viewValue: 'Taxed By Tax 5'},
	    {value: 'Taxed By Tax-6', viewValue: 'Taxed By Tax 6'},
	    {value: 'Taxed By Tax-7', viewValue: 'Taxed By Tax 7'},
	    {value: 'Taxed By Tax-8', viewValue: 'Taxed By Tax 8'},
	    {value: 'Taxed By Tax-9', viewValue: 'Taxed By Tax 9'},
	    {value: 'Taxed By Tax-10', viewValue: 'Taxed By Tax 10'},
	    {value: 'Taxed By Tax-11', viewValue: 'Taxed By Tax 11'},
	    {value: 'Taxed By Tax-12', viewValue: 'Taxed By Tax 12'},
	    {value: 'Taxed By Tax-13', viewValue: 'Taxed By Tax 13'},
	    {value: 'Taxed By Tax-14', viewValue: 'Taxed By Tax 14'},
	    {value: 'Taxed By Tax-15', viewValue: 'Taxed By Tax 15'}
	  ];

  constructor(public dialogRef: MatDialogRef<TaxesComponent>) { }

  ngOnInit() {

  }

  close(): void {
    this.dialogRef.close();
  }

}
