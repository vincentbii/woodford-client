import { Component, OnInit } from '@angular/core';
import { 
	MatCardModule,
	MatSelect, 
	MatOption, 
	MatDialog, 
	MatDialogRef
	 } from '@angular/material';
import { TaxesComponent } from '../taxes/taxes.component';
export interface Food {
  value: string;
  viewValue: string;
}


@Component({
  selector: 'app-taxable',
  templateUrl: './taxable.component.html',
  styleUrls: ['./taxable.component.css']
})
export class TaxableComponent implements OnInit {

	foods: Food[] = [
	    {value: 'VAT', viewValue: 'VAT'},
	    {value: 'VAT-2', viewValue: 'VAT 2'},
	    {value: 'VAT-3', viewValue: 'VAT 3'},
	    {value: 'VAT-4', viewValue: 'VAT 4'},
	    {value: 'VAT-5', viewValue: 'VAT 5'},
	    {value: 'VAT-6', viewValue: 'VAT 6'},
	    {value: 'VAT-7', viewValue: 'VAT 7'},
	    {value: 'VAT-8', viewValue: 'VAT 8'},
	    {value: 'VAT-9', viewValue: 'VAT 9'},
	    {value: 'VAT-10', viewValue: 'VAT 10'},
	    {value: 'VAT-11', viewValue: 'VAT 11'},
	    {value: 'VAT-12', viewValue: 'VAT 12'},
	    {value: 'VAT-13', viewValue: 'VAT 13'},
	    {value: 'VAT-14', viewValue: 'VAT 14'},
	    {value: 'VAT-15', viewValue: 'VAT 15'},
	    {value: 'Surcharge', viewValue: 'Surcharge'},
	  ];


  constructor(public dialogRef: MatDialogRef<TaxesComponent>) { }

  ngOnInit() {

  }

  close(): void {
    this.dialogRef.close();
  }
  	

}
