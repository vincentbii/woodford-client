import { Component, OnInit } from '@angular/core';
import { 
	MatCardModule,
	MatSelect, 
	MatOption, 
	MatDialog,
	MatFormField,
	MatDialogRef,
	MatDatepicker
	 } from '@angular/material';
import { TaxesComponent } from '../taxes/taxes.component';

@Component({
  selector: 'app-surcharge',
  templateUrl: './surcharge.component.html',
  styleUrls: ['./surcharge.component.css']
})
export class SurchargeComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<TaxesComponent>) { }

  ngOnInit() {

  }

  close(): void {
    this.dialogRef.close();
  }

}
