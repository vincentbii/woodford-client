import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxesMainComponent } from './taxes-main.component';

describe('TaxesMainComponent', () => {
  let component: TaxesMainComponent;
  let fixture: ComponentFixture<TaxesMainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaxesMainComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxesMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
