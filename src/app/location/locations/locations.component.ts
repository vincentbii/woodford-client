import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { 
  MatCardModule,
  MatSelect,
  MatDialog,
  MatOption,
  MatDialogRef,
  MatFormField,
  MatDatepicker
   } from '@angular/material';
import { FormBuilder, FormGroup, Validators, FormControl, ReactiveFormsModule } from '@angular/forms';
import { AreaService, User, State, LocationService, SessionService, TimezoneService, Timezone, RegionsService, Regions, UserService, Area } from '../../core';
import { LocationFooterComponent } from '../../location-footer/location-footer.component';

@Component({
	providers:[LocationFooterComponent],
  selector: 'app-locations',
  templateUrl: './locations.component.html',
  styleUrls: ['./locations.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class LocationsComponent implements OnInit {

  area: Area = {} as Area;
  location: Location = {} as Location;
  areas = [];
  errorMessage = '';
  locForm: FormGroup;

  constructor(
  	private fb: FormBuilder,
	private router: Router,
	private route: ActivatedRoute,
      private regionsService: RegionsService,
      private areaService: AreaService,
      private locationService: LocationService,
      private sessionService: SessionService,
      private userService: UserService,
      private timezoneService: TimezoneService,
      private footerComponent: LocationFooterComponent) {

  	this.locForm = this.fb.group({
  		id:'',
  		loc_code: '',
  		area_id: '',
  		minimum_age: '',
  		change_age: ''
  	});
  }


  ngOnInit() {
  	this.locForm.reset();
  	this.fetchData();
  }

  fetchData(){
  	this.areaService.getAll()
  		.subscribe(res	=>	{
  			this.areas = res;
  		});
  }

  newLocation(){
    this.errorMessage = '';
    this.locForm.reset();
  }

  openTaxes(){
    this.footerComponent.openTaxes();
  }

  openOneWayDrops(){
    this.footerComponent.openOneWayDrops();
  }

  fleetAvailability(){
    this.footerComponent.fleetAvailability();
  }

  locationNotes(){
    this.footerComponent.locationNotes();
  }

  formSetup(){
    this.footerComponent.formSetup();
  }

  estimated_monthly_expenses(){
    this.footerComponent.estimated_monthly_expenses();
  }

  submitForm(){
  	this.updateArticle(this.locForm.value);
  	// console.log(this.location)
  	this.areaService.saveLocation(this.location)
  		.subscribe(
  			res => {
	          // this.currentLocation = res;
	          this.fetchData();
	          console.log(res)
	        },
	        err  => {
	          this.errorMessage = err;
	          console.log(err)
	        }
  			);
  }

  updateArticle(values: Object) {
    Object.assign(this.location, values);
  }

}
