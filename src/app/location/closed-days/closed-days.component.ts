import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-closed-days',
  templateUrl: './closed-days.component.html',
  styleUrls: ['./closed-days.component.css']
})
export class ClosedDaysComponent implements OnInit {

  values: number[] = [102, 115, 130, 137];

  constructor() { }

  ngOnInit() {
	
  }

}
