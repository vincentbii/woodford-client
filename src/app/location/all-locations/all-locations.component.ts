import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { 
  MatCardModule,
  MatSelect,
  MatDialog,
  MatOption,
  MatDialogRef,
  MatFormField,
  MatSelectChange,
  MatDatepicker,
  MatPaginator,
  MatSort,
  MatSpinner
   } from '@angular/material';

import { AreaService, LocationService, Location, LocationApi } from '../../core/index';
import {merge, Observable, of as observableOf} from 'rxjs';
import {catchError, map, startWith, switchMap} from 'rxjs/operators';

@Component({
  selector: 'app-all-locations',
  templateUrl: './all-locations.component.html',
  styleUrls: ['./all-locations.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class AllLocationsComponent implements OnInit {

  displayedColumns: string[] = ['id', 'loc_code', 'location_name', 'street1', 'city', 'country', 'phone_number', 'status', 'hidden'];
  data: Location[] = [];

  resultsLength = 0;
  isLoadingResults = true;
  isRateLimitReached = false;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;


  constructor(
  	public dialogRef: MatDialogRef<AllLocationsComponent>,
  	private locService: LocationService
  	) { }

  locations = [];

  ngOnInit() {
  	this.fetchData();

  	this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

  	merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        startWith({}),
        switchMap(() => {
          this.isLoadingResults = true;
          return this.locService!.getAllLocations(
            this.sort.active, this.sort.direction, this.paginator.pageIndex);
        }),
        map(data => {
          // Flip flag to show that loading has finished.
          this.isLoadingResults = false;
          this.isRateLimitReached = false;
          this.resultsLength = data.total;

          return data.data;
        }),
        catchError(() => {
          this.isLoadingResults = false;
          // Catch if the GitHub API has reached its rate limit. Return empty data.
          this.isRateLimitReached = true;
          return observableOf([]);
        })
      ).subscribe(data => {
      	this.data = data;
      	// console.log(data)
      });

  }

  close(): void {
    this.dialogRef.close();
  }

  fetchData(){
  	this.locService.getAll()
  	.subscribe(data	=>	{
  		this.locations = data;
  		// console.log(data)
  	})
  }

}
