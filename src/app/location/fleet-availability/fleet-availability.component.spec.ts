import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FleetAvailabilityComponent } from './fleet-availability.component';

describe('FleetAvailabilityComponent', () => {
  let component: FleetAvailabilityComponent;
  let fixture: ComponentFixture<FleetAvailabilityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FleetAvailabilityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FleetAvailabilityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
