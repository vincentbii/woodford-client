import { Component, OnInit } from '@angular/core';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { 
  MatCardModule,
  MatSelect,
  MatDialog,
  MatOption,
  MatDialogRef,
  MatFormField,
  MatDatepicker
   } from '@angular/material';
import { FormBuilder, FormGroup, FormControl, ReactiveFormsModule } from '@angular/forms';
import {
	AreaService,
	User,
	State,
	LocationService,
	SessionService,
	TimezoneService,
	Timezone,
	RegionsService,
	Regions,
	UserService,
	Area } from '../../core';

@Component({
  selector: 'app-fleet-availability',
  templateUrl: './fleet-availability.component.html',
  styleUrls: ['./fleet-availability.component.css']
})
export class FleetAvailabilityComponent implements OnInit {
	availability = [];
	availabilityForm: FormGroup;
  constructor(
  	private areaService: AreaService,
  	public dialogRef: MatDialogRef<FleetAvailabilityComponent>,
  	private fb: FormBuilder,
  	) {
  	this.availabilityForm = this.fb.group({
      allowed: '',
      drop_fee: ''
    });
  }

  ngOnInit() {
  	this.fetchData();
  }


  close(): void {
    this.dialogRef.close();
  }

  fetchData(){
  	this.areaService.getFleetAvailabilityShares()
  		.subscribe(res => {
  			this.availability = res;
  		})
  }

}
