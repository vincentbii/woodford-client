import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OneWayDropsComponent } from './one-way-drops.component';

describe('OneWayDropsComponent', () => {
  let component: OneWayDropsComponent;
  let fixture: ComponentFixture<OneWayDropsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OneWayDropsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OneWayDropsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
