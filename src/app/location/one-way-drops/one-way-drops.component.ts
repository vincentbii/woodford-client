import { Component, OnInit } from '@angular/core';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { 
  MatCardModule,
  MatSelect,
  MatDialog,
  MatOption,
  MatDialogRef,
  MatFormField,
  MatDatepicker
   } from '@angular/material';
import { FormBuilder, FormGroup, FormControl, ReactiveFormsModule } from '@angular/forms';
import {
	AreaService,
	LocationService
} from '../../core';


@Component({
  selector: 'app-one-way-drops',
  templateUrl: './one-way-drops.component.html',
  styleUrls: ['./one-way-drops.component.css']
})
export class OneWayDropsComponent implements OnInit {

  drops = [];
  dropsForm: FormGroup;

  constructor(
  	public dialogRef: MatDialogRef<OneWayDropsComponent>,
  	private areaService: AreaService,
  	private fb: FormBuilder,
  	private locationService: LocationService
  	) {
  	this.dropsForm = this.fb.group({
      allowed: '',
      drop_fee: ''
    });
  }

  ngOnInit() {
  	this.fetchData();
  }

  close(): void {
    this.dialogRef.close();
  }

  fetchData(){
  	this.areaService.getOneWayDrops()
  		.subscribe(res	=>	{
  			this.drops = res;
  		})
  }

  submit(){
    this.updateArticle(this.dropsForm.value);
    console.log(this.dropsForm.value)
  }

  updateArticle(values: Object) {
    Object.assign(this.drops, values);
  }
}
