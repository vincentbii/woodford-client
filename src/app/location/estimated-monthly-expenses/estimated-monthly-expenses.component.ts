import { Component, OnInit } from '@angular/core';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { 
  MatCardModule,
  MatSelect,
  MatDialog,
  MatOption,
  MatDialogRef,
  MatFormField,
  MatDatepicker
   } from '@angular/material';
import { FormBuilder, FormGroup, FormControl, ReactiveFormsModule } from '@angular/forms';
import {
	AreaService,
	User,
	State,
	LocationService,
	SessionService,
	TimezoneService,
	Timezone,
	RegionsService,
	Regions,
	UserService,
	Area } from '../../core';

  @Component({
  selector: 'app-estimated-monthly-expenses',
  templateUrl: './estimated-monthly-expenses.component.html',
  styleUrls: ['./estimated-monthly-expenses.component.css']
})
export class EstimatedMonthlyExpensesComponent implements OnInit {

  	expenses = [];
	expensesForm: FormGroup;
  constructor(
  	private areaService: AreaService,
  	public dialogRef: MatDialogRef<EstimatedMonthlyExpensesComponent>,
  	private fb: FormBuilder,
  	) {
  	this.expensesForm = this.fb.group({
      location: this.fb.group({
      	loc_code: '',
      	location_name: ''
      }),
      amount: ''
    });
  }

  ngOnInit() {
  	this.fetchData();
  }


  close(): void {
    this.dialogRef.close();
  }

  fetchData(){
  	this.areaService.getEstimatedExpenses()
	.subscribe(res => {
		this.expenses = res;
	})
  }

  populateForm(expenses){
  	this.expensesForm.patchValue(expenses);
  }

}
