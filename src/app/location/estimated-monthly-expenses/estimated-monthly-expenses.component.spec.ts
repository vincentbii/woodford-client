import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EstimatedMonthlyExpensesComponent } from './estimated-monthly-expenses.component';

describe('EstimatedMonthlyExpensesComponent', () => {
  let component: EstimatedMonthlyExpensesComponent;
  let fixture: ComponentFixture<EstimatedMonthlyExpensesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EstimatedMonthlyExpensesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EstimatedMonthlyExpensesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
