import { Component, OnInit } from '@angular/core';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { 
  MatCardModule,
  MatSelect,
  MatDialog,
  MatOption,
  MatDialogRef,
  MatFormField,
  MatSelectChange,
  MatDatepicker
   } from '@angular/material';
import { FormBuilder, FormGroup, FormControl, ReactiveFormsModule } from '@angular/forms';
import {
	AreaService,
	LocationService
} from '../../core';

@Component({
  selector: 'app-form-setup',
  templateUrl: './form-setup.component.html',
  styleUrls: ['./form-setup.component.css']
})
export class FormSetupComponent implements OnInit {

  formSetp = [];
  dealsSetp = [];
  formFiles = [];
  setupForm: FormGroup;
  dealsForm: FormGroup;
  deals = [];
  errorMessage = '';

  constructor(
  	public dialogRef: MatDialogRef<FormSetupComponent>,
  	private areaService: AreaService,
  	private fb: FormBuilder,
  	private locationService: LocationService
  	) {
  	this.setupForm = this.fb.group({
      file: '',
      description: '',
      deal:{
      	name: '',
      },
      language: '',
    });


	this.dealsForm = this.fb.group({
      name: ''
    });
}

  ngOnInit() {
  	this.fetchData();
  }

  close(): void {
    this.dialogRef.close();
  }

  fetchData(){
  	this.areaService.getDeals()
  		.subscribe(res	=>	{
  			this.deals = res;
  		});

  	
  }

  submit(){
    this.updateArticle(this.setupForm.value);
  }

  objChanged(event){
  	this.areaService.getsFormSetup(event)
		.subscribe(res	=>	{
			this.formFiles = res;
		})
  }

  updateArticle(values: Object) {
    Object.assign(this.formSetp, values);
  }

  populateForm(file){
  	this.setupForm.patchValue(file);
  }

  delete(file){
  	if(confirm("Are you sure to delete "+file.description)) {
      this.areaService.destroy(file.id)
        .subscribe(
          data  =>  {
            this.fetchData();
          },
          err  =>  {
            this.errorMessage = err.errors;
            console.log(this.errorMessage)
          }
          );
    }
  }

}
