import { Component, OnInit } from '@angular/core';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { 
  MatCardModule,
  MatSelect,
  MatDialog,
  MatOption,
  MatDialogRef,
  MatFormField,
  MatDatepicker
   } from '@angular/material';
import { FormBuilder, FormGroup, FormControl, ReactiveFormsModule } from '@angular/forms';
import {
	AreaService,
	LocationService,
	NoteType,
	LocationNotes
} from '../../core';


@Component({
  selector: 'app-location-notes',
  templateUrl: './location-notes.component.html',
  styleUrls: ['./location-notes.component.css']
})
export class LocationNotesComponent implements OnInit {

  locationNotes = [];
  locationNotesForm: FormGroup;
  noteTypes = [];
  languages = [];

  constructor(
  	public dialogRef: MatDialogRef<LocationNotesComponent>,
  	private areaService: AreaService,
  	private fb: FormBuilder,
  	private locationService: LocationService
  	) {
  	this.locationNotesForm = this.fb.group({
      language: '',
      note_type:'',
      location: this.fb.group({
      	loc_code: '',
      }),
      note:''
    });
  }

  ngOnInit() {
  	this.fetchData();
  }

  close(): void {
    this.dialogRef.close();
  }

  fetchData(){
  	this.areaService.getNoteType()
  		.subscribe(res	=>	{
  			this.noteTypes = res;
  		});

  	this.areaService.getLanguage()
  		.subscribe(res	=>	{
  			this.languages = res;
  		});
  	this.areaService.getLocationNotes()
  		.subscribe(res	=>	{
  			this.locationNotes = res;
  			this.locationNotesForm.patchValue(this.locationNotes[0]);
  		})
  }

}
