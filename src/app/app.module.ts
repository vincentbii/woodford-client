import { ModuleWithProviders, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { TextInputAutocompleteModule } from 'angular-text-input-autocomplete';
import {
  MatButtonModule,
  MatDialogModule,
  MatDatepickerModule,
  MatSelectModule,
  MatInputModule,
  MatNativeDateModule,
  MatOptionModule,
  MatIconModule, MatTabsModule,
  MatSnackBarModule,
  MatAutocompleteModule,
  MatProgressSpinnerModule,
  MatCheckboxModule,
  MatMenuModule,
  MatCardModule,
  MatFormFieldModule,
  MatPaginatorModule,
  MatTableModule,
  MatSortModule
} from '@angular/material';
import { AppComponent } from './app.component';
import { AuthModule } from './auth/auth.module';
import { HomeModule } from './home/home.module';
import {
  FooterComponent,
  HeaderComponent,
  SharedModule
} from './shared';
import { jqxCalendarComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxcalendar';
import { RegionModule } from './region/region.module';
import { RegionsService, LocationService } from './core';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { CoreModule } from './core/core.module';
import { DataTablesModule } from 'angular-datatables';
import * as jquery from 'jquery';
import { AreaComponent } from './area/area.component';
import { ListErrorsComponent } from './shared';
import { LocationComponent } from './location/location.component';
import { LocationMainComponent } from './location-main/location-main.component';
import { LocationOptionsComponent } from './location-options/location-options.component';
import { LocationFormComponent } from './location-form/location-form.component';
import { LocationRenterComponent } from './location-renter/location-renter.component';
import { LocationFooterComponent } from './location-footer/location-footer.component';
import { TaxesMainComponent } from './taxes-main/taxes-main.component';
import { TaxesComponent } from './Taxes/taxes/taxes.component';
import { TaxableComponent } from './Taxes/taxable/taxable.component';
import { TaxesTaxedByComponent } from './Taxes/taxes-taxed-by/taxes-taxed-by.component';
import { SurchargeComponent } from './Taxes/surcharge/surcharge.component';
import { OrderByComponent } from './Taxes/order-by/order-by.component';
import { OneWayDropsComponent } from './location/one-way-drops/one-way-drops.component';
import { FleetAvailabilityComponent } from './location/fleet-availability/fleet-availability.component';
import { FormSetupComponent } from './location/form-setup/form-setup.component';
import { EstimatedMonthlyExpensesComponent } from './location/estimated-monthly-expenses/estimated-monthly-expenses.component';
import { HoursOfOperationComponent } from './location/hours-of-operation/hours-of-operation.component';
import { ClosedDaysComponent } from './location/closed-days/closed-days.component';
import { LocationsComponent } from './location/locations/locations.component';
import { AllLocationsComponent, LocationNotesComponent } from './location/index';
@NgModule({
  declarations: [
  AppComponent,
  jqxCalendarComponent,
  FooterComponent, 
  HeaderComponent, 
  AreaComponent, 
  LocationComponent, 
  LocationMainComponent, 
  LocationOptionsComponent, 
  LocationFormComponent, 
  LocationRenterComponent, 
  LocationFooterComponent, 
  TaxesMainComponent, 
  TaxesComponent, 
  TaxableComponent, 
  TaxesTaxedByComponent,
  FleetAvailabilityComponent,
  SurchargeComponent, 
  OrderByComponent, 
  OneWayDropsComponent, LocationNotesComponent, FormSetupComponent, EstimatedMonthlyExpensesComponent, HoursOfOperationComponent, ClosedDaysComponent, LocationsComponent, AllLocationsComponent],
  imports: [
    BrowserModule,
    CoreModule,
    MatCardModule,
    MatIconModule,
    MatTableModule,
    MatSortModule,
    DataTablesModule,
    MatNativeDateModule,
    MatInputModule,
    MatSelectModule,
    MatOptionModule,
    MatMenuModule,
    MatProgressSpinnerModule,
    MatButtonModule,
    MatDatepickerModule,
    MatFormFieldModule,
    SharedModule,
    MatCheckboxModule,
    BrowserAnimationsModule,
    TextInputAutocompleteModule,
    MatAutocompleteModule,
    HomeModule,
    AuthModule,
    MatTabsModule,
    RegionModule,
    MatDialogModule,
    AppRoutingModule,
    MatPaginatorModule,

  ],
  entryComponents: [
    TaxesMainComponent,
    OneWayDropsComponent,
    FleetAvailabilityComponent,
    LocationNotesComponent,
    FormSetupComponent,
    EstimatedMonthlyExpensesComponent,AllLocationsComponent
  ],
  providers: [LocationService],
  bootstrap: [AppComponent]
})
export class AppModule {}
