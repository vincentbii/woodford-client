import { Component, OnInit } from '@angular/core';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatCardModule} from '@angular/material/card';
import { RegionsService, UserService, Regions, Errors, User } from '../core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, FormControl, ReactiveFormsModule } from '@angular/forms';
import { MatIcon } from '@angular/material';
import 'rxjs/Rx';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-region',
  templateUrl: './region.component.html',
  styleUrls: ['./region.component.css']
})
export class RegionComponent implements OnInit {
	region: Regions = {} as Regions;
	regionForm: FormGroup;
	errorMessage = '';
   isDeleting = false;
   currentUser:User;
  constructor(
  	private router: Router,
    private route: ActivatedRoute,
    private regionService: RegionsService,
    private userService: UserService,
    private fb: FormBuilder
  	) {
  	this.regionForm = this.fb.group({
      id:  '',
      code: '',
      description: '',
      contact: '',
      street1: '',
      street2: '',
      city: '',
      postal_code: '',
      phone: '',
      state: '',
    });
  }

  isAuthenticated: boolean;
	regions: Array<string> = [];
	isSubmitting = false;
  edit = false;

  ngOnInit() {
    this.userService.currentUser.subscribe(
      (userData) => {
        this.currentUser = userData;
      }
    );

  	this.route.data.subscribe((data: { region: Regions }) => {
      if (data.region) {
        this.region = data.region;
        this.regionForm.patchValue(data.region);
      }
    });
    
    this.fetchData();

  }

  fetchData() {
    this.regionService.getAll()
      .subscribe(regions => {
        this.regions = regions;
      });
  }

  populateForm(region){
    this.reset();
    this.regionForm.patchValue(region);
    this.edit = true;
  }

  delete(region) {
    if(confirm("Are you sure to delete "+region.description)) {
      this.regionService.destroy(region.id)
        .subscribe(
          data  =>  {
            this.fetchData();
          },
          err  =>  {
            this.errorMessage = err.errors.message;
            console.log(this.errorMessage)
          }
          );
    }
  }

  reset(){
    this.errorMessage = '';
    this.regionForm.reset();
  }

  submitForm() {
    this.errorMessage = '';
    this.isSubmitting = true;
    this.updateArticle(this.regionForm.value);
    this.regionService.save(this.region)
      .subscribe(
        res => {
          this.fetchData();
        },
        err  => {
          this.errorMessage = err.errors.message;
        }
        );
      this.isSubmitting = false;
  }

  updateArticle(values: Object) {
    Object.assign(this.region, values);
  }

}
