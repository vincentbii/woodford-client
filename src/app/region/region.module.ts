import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RegionResolverService } from  './region-resolver.service';
import { RegionComponent } from './region.component';
import {MatButtonModule, MatIconModule, MatProgressSpinner, MatCheckboxModule, MatMenuModule, MatCardModule, MatFormFieldModule, MatSpinner} from '@angular/material';
import { FormBuilder, FormGroup, FormControl, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    MatCardModule,
    MatIconModule,
    MatMenuModule,
    MatButtonModule,
    MatFormFieldModule,
    MatCheckboxModule,
    ReactiveFormsModule,
  ],
  declarations: [RegionComponent],
  providers: [RegionResolverService]
})
export class RegionModule { }
