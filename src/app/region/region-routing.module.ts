import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RegionResolverService } from './region-resolver.service';
import { RegionComponent } from './region.component';

const routes: Routes = [
  {
    path: 'regions',
    component: RegionComponent,
    resolve: {
      isAuthenticated: RegionResolverService
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RegionRoutingModule { }
