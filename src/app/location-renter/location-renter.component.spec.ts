import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LocationRenterComponent } from './location-renter.component';

describe('LocationRenterComponent', () => {
  let component: LocationRenterComponent;
  let fixture: ComponentFixture<LocationRenterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LocationRenterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LocationRenterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
