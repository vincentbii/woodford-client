import { NgModule } from '@angular/core';
import { AuthGuard } from './core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { RegionComponent } from './region/region.component';
import { AreaComponent } from './area/area.component';
import { LocationComponent } from './location/location.component';
import { HoursOfOperationComponent } from './location/hours-of-operation/hours-of-operation.component';
import { ClosedDaysComponent } from './location/closed-days/closed-days.component';
const routes: Routes = [
  {
    path: 'settings',
    loadChildren: './settings/settings.module#SettingsModule'
  },
  {
    path: 'area',
    component: AreaComponent
  },
  {
    path: 'regions',
    // loadChildren: './region/region.module#RegionModule'
    component: RegionComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'login',
    loadChildren: './auth/auth.module#AuthModule'
  },
  {
    path: 'location',
    component: LocationComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'hooperation',
    component: HoursOfOperationComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'closed_days',
    component: ClosedDaysComponent,
    canActivate: [AuthGuard]
  },
  {
    path: '',
    redirectTo: '/login',
    pathMatch: 'full'
  },
  { path: '**', loadChildren: './auth/auth.module#AuthModule' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    // preload all modules; optionally we could
    // implement a custom preloading strategy for just some
    // of the modules (PRs welcome 😉)
    preloadingStrategy: PreloadAllModules
  })],
  exports: [RouterModule]
})
export class AppRoutingModule {}
