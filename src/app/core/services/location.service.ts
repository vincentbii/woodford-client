import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Regions, LocationApi } from '../models';
import { ApiService } from './api.service';
import { map } from 'rxjs/operators';
import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable()
export class LocationService {

  constructor(
    private apiService: ApiService,
    private http: HttpClient
    ) { }

  getAll(): Observable<[string]> {
    return this.apiService.get('/locations')
          .pipe(map(data => data.locations));
  }

  getAllLocations(sort: string, order: string, page: number): Observable<LocationApi> {
    const href = '/locations2';
    const requestUrl =
        `${href}?sort=${sort}&order=${order}&page=${page + 1}`;

    // return this.apiService.get('/locations2?sort=${sort}&order=${order}&page=${page + 1}');
    return this.http.get<LocationApi>(`${environment.api_url}${requestUrl}`);
  }

  getDefaultRatePlans(): Observable<[string]>{
    return this.apiService.get('/default_rate_plans')
      .pipe(map(data  =>  data));
  }

  getRateCodes(): Observable<[string]>{
    return this.apiService.get('/rate_code')
      .pipe(map(data  =>  data));
  }

  getlblUnitStatusAfterReturn(): Observable<[string]>{
    return this.apiService.get('/lblUnitStatusAfterReturn')
    .pipe(map(data  =>  data));
  }

  save(location): Observable<Location> {

  	if (location.id !== null) {
      return this.apiService.put('/locations/' + location.id, {location: location})
        .pipe(map(data => data));

    } else {
      return this.apiService.post('/locations/', {location: location})
        .pipe(map(data => data));
    }

  }

  destroy(region) {
    return this.apiService.delete('/locations/' + location);
  }
}
