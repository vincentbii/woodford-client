import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Regions } from '../models';
import { LocationSesion } from '../models';
import { ApiService } from './api.service';
import { map } from 'rxjs/operators';

@Injectable()
export class RegionsService {

  constructor(private apiService: ApiService) {}

  getAll(): Observable<[string]> {
    return this.apiService.get('/regions')
          .pipe(map(data => data.regions));
  }

  getTimezones(): Observable<[string]> {
    return this.apiService.get('/timezones')
          .pipe(map(data => data.timezones));
  }

  getLocations(): Observable<[string]> {
    return this.apiService.get('/locations')
          .pipe(map(data => data.locations));
  }

  save(region): Observable<Regions> {

  	if (region.id) {
      return this.apiService.put('/regions/' + region.id, {region: region})
        .pipe(map(data => data));

    // Otherwise, create a new article
    } else {
      return this.apiService.post('/regions/', {region: region})
        .pipe(map(data => data.article));
    }


      // return this.apiService.post('/regions/', {region: region})
      //   .pipe(map(data => data.region));
  }

  saveLocSessions(session): Observable<LocationSesion> {
      return this.apiService.post('/sessions/', {session: session})
        .pipe(map(data => data.session));


      // return this.apiService.post('/regions/', {region: region})
      //   .pipe(map(data => data.region));
  }

  destroy(region) {
    return this.apiService.delete('/regions/' + region);
  }
}
