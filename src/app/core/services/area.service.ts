import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';

import { ApiService } from './api.service';
import { Area, Location } from '../models';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class AreaService {

  constructor(private apiService: ApiService) { }

  getAll(): Observable<[string]> {
    return this.apiService.get('/areas')
          .pipe(map(data => data.areas));
  }

  getPaymentDueAction(): Observable<[string]>{
    return this.apiService.get('/payment_due_action').pipe(map(data  =>  data));
  }

  getEstimatedExpenses(): Observable<[string]>{
    return this.apiService.get('/expenses').pipe(map(data  =>  data));
  }

  getDeals(): Observable<[string]>{
    return this.apiService.get('/deals').pipe(map(data  =>  data.deals));
  }

  getsFormSetup(deal): Observable<[string]>{
    return this.apiService.get('/formSetup/'+deal).pipe(map(data  =>  data.setup));
  }

  getCities(): Observable<[string]>{
    return this.apiService.get('/cities').pipe(map(data  =>  data.cities));
  }

  getCountries(): Observable<[string]>{
    return this.apiService.get('/countries').pipe(map(data  =>  data.countries));
  }

  getOneWayDrops(): Observable<[string]>{
    return this.apiService.get('/onewaydrops').pipe(map(data  =>  data));
  }

  getNoteType(): Observable<[string]>{
    return this.apiService.get('/noteTypes').pipe(map(data  =>  data.types));
  }

  getLanguage(): Observable<[string]>{
    return this.apiService.get('/noteTypes').pipe(map(data  =>  data.language));
  }

  getLocationNotes(): Observable<[string]>{
    return this.apiService.get('/LocationNotes').pipe(map(data  =>  data));
  }

  getFleetAvailabilityShares(): Observable<[string]>{
    return this.apiService.get('/fleet').pipe(map(data  =>  data));
  }

  getStates(): Observable<[string]>{
    return this.apiService.get('/states').pipe(map(data  =>  data.states));
  }

  getTimezones(): Observable<[string]>{
    return this.apiService.get('/timezones').pipe(map(data  =>  data));
  }

  getCurrency(): Observable<[string]>{
    return this.apiService.get('/currency').pipe(map(data  => data));
  }

  save(area): Observable<Area> {

  	if (area.id) {
      return this.apiService.put('/areas/' + area.id, {area: area})
        .pipe(map(data => data));

    // Otherwise, create a new article
    } else {
      return this.apiService.post('/areas/', {area: area})
        .pipe(map(data => data.area));
    }


      // return this.apiService.post('/regions/', {region: region})
      //   .pipe(map(data => data.region));
  }

  saveLocation(location): Observable<Location> {

    if (location.id !== null) {
      return this.apiService.put('/locations/' + location.id, {location: location})
        .pipe(map(data => data));

    } else {
      return this.apiService.post('/locations', {location: location})
        .pipe(map(data => data));
    }

  }

  destroy(area) {
    return this.apiService.delete('/areas/' + area);
  }
}
