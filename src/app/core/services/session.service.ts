import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Regions } from '../models';
import { LocationSesion } from '../models';
import { ApiService } from './api.service';
import { map } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class SessionService {

  constructor(private apiService: ApiService) { }

  getSession(user): Observable<[string]>{
  	return this.apiService.get('/sessions/'+user).pipe(map(data=>data.sessions));
  }

  save(session, user): Observable<LocationSesion> {
      return this.apiService.put('/sessions/' + session, {user:user})
        .pipe(map(data => data));
  }
}
