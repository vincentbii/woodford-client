import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { Timezone } from '../models';
import { ApiService } from './api.service';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TimezoneService {

  constructor(private apiService: ApiService) { }

  getAll(): Observable<[string]> {
    return this.apiService.get('/timezones')
          .pipe(map(data => data));
  }
}
