import { Regions } from './regions.model';

export interface Area {
  id: number;
  code: string;
  description: string;
  region: Regions;
  contact: string;
  street1: string;
  street2: string;
  postal_code: string;
  city: string;
  state: string;
}
