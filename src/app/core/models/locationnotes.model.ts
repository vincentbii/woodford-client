import { Location, NoteType} from './index';
export interface LocationNotes {
	location: Location;
	language: string;
	noteType: NoteType;
	note: string;
}