import { Deals } from './deals.model';
export interface formSetup {
	id: number;
	file: string;
	description: string;
	deal: Deals;
	language: string;
}