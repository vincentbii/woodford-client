import { Area, State, Timezone } from './index';
export interface Location {
  id:  number;
  location_name: string;
  country: string;
  convert_rez_to_local_currency: number,
  area_id: Area;
  dba_name: string;
  minimum_age: number;
  change_age: boolean;
  franchise_type: number;
  ra_number: number;
  grace_period: number;
  show_grace_period: boolean;
  property_id: number;
  government_id: number;
  location_email: string;
  days_in_rental_month: number;
  fax_number: number;
  purge_years: number;
  rem_days:number;
  require_refund: boolean;
  also_apply: boolean;
  loc_code: string;
  email_bcc: string;
  payment_due_action_at_edit: number;
  payment_due_action_at_return: number;
  payment_due_action_at_open: number;
  physical_inventory_group: string;
  contact: string;
  street1: string;
  require_payment: boolean;
  street2: string;
  send_rez_email: boolean;
  city: string;
  postal_code: number;
  phone_number: number;
  state: State;
  currency: string;
  next_day_cutoff: number;
  timezone: Timezone;
}

export interface LocationApi {
  data: Location[];
  total: number;
}