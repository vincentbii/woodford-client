export interface lblUnitStatusAfterReturn {
	id: number;
	name: string;
	status: string;
}