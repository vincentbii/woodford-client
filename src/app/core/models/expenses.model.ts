import { Location } from './location.model';

export interface Expenses {
  id: number;
  amount: number;
  location: Location;
}
