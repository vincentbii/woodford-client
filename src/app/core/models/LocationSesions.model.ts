import { Location } from './location.model';
export interface LocationSesion {
  user: string;
  location: Location;
}
