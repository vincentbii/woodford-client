export interface Regions {
  id: number;
  code: string;
  description: string;
  contact: string;
  street1: string;
  street2: string;
  postal_code: string;
  city: string;
  state: string;
}
