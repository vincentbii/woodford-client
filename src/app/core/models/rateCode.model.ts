export interface rateCode {
	id: number;
	name: string;
	rate: string;
}