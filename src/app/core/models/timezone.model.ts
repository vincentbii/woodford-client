import { Zone } from './zone.model';
export interface Timezone {
  zone: Zone;
  dst: number;
  abbreviation: string;
  timestart: string;
  gmt_offset: string;
}